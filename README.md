# SmartFlix

## Descrição
Esse projeto foi criado para participar de um processo seletivo da Smartpay

## Funcionalidades
- Filmes melhores avaliados
- Filmes mais populares
- Lista dos próximos filmes nos cinemas.
- Lista de filmes em cartaz.
- Procurar filme pelo nome
- Lista "infinita" de filmes

## Tecnologias

>Packages
- cupertino_icons: ^1.0.2
- get: ^4.6.1
- dio: ^4.0.4
- custom_navigation_bar: ^0.8.2
- google_fonts: ^2.2.0
- material_floating_search_bar: ^0.3.6
- font_awesome_flutter: ^9.2.0
- url_launcher: ^6.0.17

>Framework
- Flutter 2.5.3
- Dart version 2.14.4

## Inicialização

``` dart
flutter pub get
flutter run
```

## Status do projeto
> Finalizado

--- 
## Download do projeto
https://drive.google.com/file/d/1Doz8cBAHMXNaPxlXOOjfcpYjOWnXyxjU/view?usp=sharing