import 'package:app_movies/env.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';

const dynamic param = {"api_key": apikeyTheMovieDatabase};

Future getDiscover(int page) async {
  try {
    var response = await Dio().get(urlHost + endpointDiscoverMovie,
        queryParameters: {"api_key": apikeyTheMovieDatabase, "page": page});
    if (response.statusCode == 200) {
      return response.data['results'];
    }
    return [];
  } catch (e) {
    debugPrint("$e");
  }
}

Future getNowPlaying() async {
  try {
    var response =
        await Dio().get(urlHost + endpointNowPlaying, queryParameters: param);
    if (response.statusCode == 200) {
      return response.data['results'];
    }
    return [];
  } catch (e) {
    debugPrint("$e");
  }
}

Future getTopRated() async {
  try {
    var response =
        await Dio().get(urlHost + endpointTopRated, queryParameters: param);

    if (response.statusCode == 200) {
      return response.data['results'];
    }

    return [];
  } catch (e) {
    debugPrint("$e");
  }
}

Future getMostPopular() async {
  try {
    var response =
        await Dio().get(urlHost + endpointMostPopular, queryParameters: param);

    if (response.statusCode == 200) {
      return response.data['results'];
    }

    return [];
  } catch (e) {
    debugPrint("$e");
  }
}

Future getUpcoming() async {
  try {
    var response =
        await Dio().get(urlHost + endpointUpcoming, queryParameters: param);

    if (response.statusCode == 200) {
      return response.data['results'];
    }

    return [];
  } catch (e) {
    debugPrint("$e");
  }
}

Future getGenresMovie() async {
  try {
    var response =
        await Dio().get(urlHost + endpointGenresList, queryParameters: param);
    if (response.statusCode == 200) {
      return response.data['genres'];
    }
    return [];
  } catch (e) {
    debugPrint("$e");
  }
}

Future getSearchMovie(query) async {
  try {
    var response = await Dio().get(urlHost + endpointSearchMovie,
        queryParameters: {"api_key": apikeyTheMovieDatabase, "query": query});

    if (response.statusCode == 200) {
      return response.data['results'];
    }
    return [];
  } catch (e) {
    debugPrint("$e");
  }
}
