// ignore_for_file: prefer_const_constructors

import 'package:app_movies/bindings/main_binding.dart';
import 'package:app_movies/screens/main_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';

void main() {
  SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
    statusBarColor: Colors.black87,
    statusBarIconBrightness: Brightness.light,
  ));
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      initialBinding: MainBinding(),
      getPages: _getRoutes(),
      defaultTransition: Transition.native,
      title: 'SmartFlix',
      theme: ThemeData(
        primarySwatch: Colors.lightBlue,
        backgroundColor: Colors.black87,
      ),
      home: const MainScreen(),
    );
  }
}

_getRoutes() {
  return [
    GetPage(
      name: '/',
      page: () => const MainScreen(),
      binding: MainBinding(),
    )
  ];
}
