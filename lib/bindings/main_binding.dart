import 'package:app_movies/controllers/favorite_controller.dart';
import 'package:app_movies/controllers/home_controller.dart';
import 'package:app_movies/controllers/main_controller.dart';
import 'package:app_movies/controllers/profile_controller.dart';
import 'package:app_movies/controllers/search_controller.dart';
import 'package:get/get.dart';

class MainBinding extends Bindings {
  @override
  void dependencies() {
    Get.put(MainController());
    Get.lazyPut(() => HomeController());
    Get.lazyPut(() => SearchController());
    Get.lazyPut(() => FavoriteController());
    Get.lazyPut(() => ProfileController());
  }
}
