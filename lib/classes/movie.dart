// (name, poster image, genre, overview and release date).

import 'package:app_movies/controllers/main_controller.dart';
import 'package:app_movies/env.dart';
import 'package:get/get.dart';

class Movie {
  int movieId = 0;
  String title = "";
  String posterPath = "";
  String backdropPath = "";
  List<dynamic> genres = [];
  String overview = "";
  String releaseDate = "";
  double voteAverage = 0.0;

  Movie(
      {required this.movieId,
      required this.title,
      required this.posterPath,
      required this.backdropPath,
      required this.genres,
      required this.overview,
      required this.releaseDate,
      required this.voteAverage});

  Movie.fromMap(Map map) {
    movieId = map['id'] ?? "";
    title = map['title'] ?? "";
    posterPath = map['poster_path'] ?? "";
    backdropPath = map['backdrop_path'] ?? "";
    genres = map['genre_ids'] ?? [];
    overview = map['overview'] ?? "";
    releaseDate = map['release_date'] ?? "";
    voteAverage = double.parse("${map['vote_average']}");
  }

  Map<String, dynamic> toJson() => {
        'movieId': movieId,
        'title': title,
        'posterPath': posterPath,
        'backdropPath': backdropPath,
        'genres': genres,
        'overview': overview,
        'releaseDate': releaseDate,
        'voteAverage': voteAverage
      };

  String get year {
    if (releaseDate.isEmpty) {
      return "";
    }
    return DateTime.parse(releaseDate).year.toString();
  }

  List get generos {
    List<String> myGenres = [];
    MainController mainController = Get.put(MainController());
    List _originaListGenres = mainController.genresList;

    for (var i in genres) {
      for (var element in _originaListGenres) {
        if (element['id'] == i) {
          myGenres.add(element['name']);
        }
      }
    }

    return myGenres;
  }

  String get getPosterPath {
    return posterPath.isEmpty
        ? "https://nachamafrica.org/wp-content/themes/consultix/images/no-image-found-360x260.png"
        : urlHostImage + posterPath;
  }

  String get getBackdropPath {
    return backdropPath.isEmpty
        ? "https://nachamafrica.org/wp-content/themes/consultix/images/no-image-found-360x260.png"
        : urlHostImage + backdropPath;
  }
}
