import 'package:app_movies/api.dart';
import 'package:app_movies/classes/movie.dart';
import 'package:app_movies/controllers/main_controller.dart';
import 'package:get/get.dart';

class HomeController extends GetxController
    with GetSingleTickerProviderStateMixin {
  MainController mainController = Get.find();

  final nowPlaying = [].obs;
  final topRated = [].obs;
  final mostPopular = [].obs;
  final upcoming = [].obs;

  @override
  Future<void> onInit() async {
    getNowPlaying().then((value) {
      value.forEach((i) {
        Movie movie = Movie.fromMap(i);
        nowPlaying.add(movie);
      });
    });
    getTopRated().then((value) {
      value.forEach((i) {
        Movie movie = Movie.fromMap(i);
        topRated.add(movie);
      });
    });
    getMostPopular().then((value) {
      value.forEach((i) {
        Movie movie = Movie.fromMap(i);
        mostPopular.add(movie);
      });
    });
    getUpcoming().then((value) {
      value.forEach((i) {
        Movie movie = Movie.fromMap(i);
        upcoming.add(movie);
      });
    });
    super.onInit();
  }
}
