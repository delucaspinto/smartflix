import 'package:app_movies/api.dart';
import 'package:get/get.dart';

class MainController extends GetxController
    with GetSingleTickerProviderStateMixin {
  final index = 0.obs;

  final genresList = [].obs;

  @override
  Future<void> onInit() async {
    await getGenresMovie().then((value) {
      genresList.value = value;
    });
    super.onInit();
  }
}
