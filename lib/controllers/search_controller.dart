import 'package:app_movies/api.dart';
import 'package:app_movies/classes/movie.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'home_controller.dart';

class SearchController extends GetxController
    with GetSingleTickerProviderStateMixin {
  HomeController homeController = Get.find();
  ScrollController scrollController = ScrollController();

  final page = 1.obs;
  List originalList = [];
  final searchList = [].obs;

  final allmovies = [].obs;
  final text = "".obs;
  // List get allmovies => _allmovies.value;

  @override
  void onInit() {
    originalList = homeController.nowPlaying;
    searchList.value = originalList;
    getDiscover(page.value).then((value) {
      value.forEach((i) {
        Movie movie = Movie.fromMap(i);
        allmovies.add(movie);
      });
    });
    scrollController.addListener(() {
      if (scrollController.position.atEdge) {
        bool isTop = scrollController.position.pixels == 0;
        if (isTop) {
        } else {
          page.value++;

          getDiscover(page.value).then((value) {
            value.forEach((i) {
              Movie movie = Movie.fromMap(i);
              allmovies.add(movie);
            });
          });
        }
        allmovies.refresh();
      }
    });

    super.onInit();
  }

  search(query) {
    getSearchMovie(query).then((value) {
      List _list = [];
      for (var i in value) {
        Movie movie = Movie.fromMap(i);
        _list.add(movie);
      }
      searchList.value = _list;
    });
  }

  getAllMovies() {
    getDiscover(page.value).then((value) {
      value.forEach((i) {
        Movie movie = Movie.fromMap(i);
        allmovies.add(movie);
      });
    });
  }
}
