const String apikeyTheMovieDatabase = "79fc335afb9508e180cb1ec24af1e0d7";
const String urlHost = "https://api.themoviedb.org/3";

const String endpointNowPlaying = "/movie/now_playing";
const String endpointTopRated = "/movie/top_rated";
const String endpointMostPopular = "/movie/popular";
const String endpointUpcoming = "/movie/upcoming";
const String endpointDiscoverMovie = "/discover/movie";

const String endpointGenresList = "/genre/movie/list";

const String endpointSearchMovie = "/search/movie";

const String urlHostImage = "https://image.tmdb.org/t/p/w500";
