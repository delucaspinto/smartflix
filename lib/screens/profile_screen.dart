import 'package:app_movies/controllers/search_controller.dart';
import 'package:app_movies/helpers/functions.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';

TextStyle _title =
    GoogleFonts.openSans(color: Colors.white38, letterSpacing: -1);
TextStyle _value = GoogleFonts.openSans(color: Colors.white, fontSize: 18);
TextStyle _verify = GoogleFonts.openSans(
    color: Colors.green, fontSize: 12, fontWeight: FontWeight.bold);
TextStyle _option =
    GoogleFonts.openSans(color: Colors.blue, fontWeight: FontWeight.bold);

class ProfileScreen extends GetWidget<SearchController> {
  const ProfileScreen({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black87,
      body: SafeArea(
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: const [
              SizedBox(height: 15),
              ListTile(
                leading: CircleAvatar(
                  radius: 30.0,
                  backgroundImage:
                      NetworkImage('https://i.imgur.com/wZRraHQ.png'),
                  backgroundColor: Colors.transparent,
                ),
                title: Text("Lucas De Lavra Pinto",
                    style: TextStyle(fontSize: 16, color: Colors.white)),
                subtitle: Text("4 de setembro de 1991",
                    style: TextStyle(fontSize: 12, color: Colors.white)),
              ),
              Redes(),
              Phone(),
              SizedBox(height: 15),
              Email(),
              SizedBox(height: 15),
              Endereco(),
            ],
          ),
        ),
      ),
    );
  }
}

class Phone extends StatelessWidget {
  const Phone({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(12.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Text("Telefone", style: _title),
          const SizedBox(height: 5),
          Text("+55 (47) 99256-1483", style: _value),
        ],
      ),
    );
  }
}

class Email extends StatelessWidget {
  const Email({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(12.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Text("Email", style: _title),
          const SizedBox(height: 5),
          Text("lucasdelavrapinto@gmail.com", style: _value),
        ],
      ),
    );
  }
}

class Endereco extends StatelessWidget {
  const Endereco({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(12.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Text("Endereço", style: _title),
          const SizedBox(height: 5),
          Text("Tijucas, SC", style: _value),
        ],
      ),
    );
  }
}

class Redes extends StatelessWidget {
  const Redes({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(12.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          // Text("Redes", style: _title),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: const [
              ButtonSocial(
                  icone: FontAwesomeIcons.instagram,
                  url: "https://www.instagram.com/lucasdellavro/",
                  cor: Colors.pinkAccent),
              ButtonSocial(
                  icone: FontAwesomeIcons.whatsapp,
                  url:
                      "https://api.whatsapp.com/send?phone=+5547992561483&text=Podemos%20conversar?",
                  cor: Colors.green),
              ButtonSocial(
                  icone: FontAwesomeIcons.linkedin,
                  url: "https://www.linkedin.com/in/lucasdelavrapinto/",
                  cor: Colors.blueAccent),
            ],
          ),
        ],
      ),
    );
  }
}

class ButtonSocial extends StatelessWidget {
  final IconData icone;

  final String url;
  final Color cor;

  const ButtonSocial(
      {Key? key, required this.icone, required this.cor, required this.url})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        launchURL(url);
      },
      child: Container(
        width: 100,
        height: 100,
        child: Icon(icone, size: 30, color: cor),
      ),
    );
  }
}
