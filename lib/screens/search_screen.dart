import 'package:app_movies/classes/movie.dart';
import 'package:app_movies/controllers/search_controller.dart';
import 'package:app_movies/screens/home_screen.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:material_floating_search_bar/material_floating_search_bar.dart';

class SearchScreen extends GetWidget<SearchController> {
  const SearchScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black87,
      body: SafeArea(
        child: Stack(
          children: [
            Container(
              margin: const EdgeInsets.only(top: 60),
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Text(
                        "All movies",
                        style: GoogleFonts.poppins(
                            fontSize: 16,
                            color: Colors.white,
                            fontWeight: FontWeight.bold),
                      ),
                    ),
                    Expanded(
                      child: Obx(() => GridView.builder(
                            controller: controller.scrollController,
                            gridDelegate:
                                const SliverGridDelegateWithMaxCrossAxisExtent(
                                    maxCrossAxisExtent: 250,
                                    childAspectRatio: 3 / 2,
                                    crossAxisSpacing: 10,
                                    mainAxisSpacing: 1),
                            itemCount: controller.allmovies.length,
                            itemBuilder: (BuildContext ctx, index) {
                              Movie movie = controller.allmovies[index];
                              return GestureDetector(
                                onTap: () =>
                                    configModalBottomSheet(context, movie),
                                child: Container(
                                  height: 250,
                                  child: Stack(
                                    children: [
                                      Image.network(
                                        movie.getBackdropPath,
                                        fit: BoxFit.cover,
                                      ),
                                      Positioned(
                                        bottom: 18,
                                        left: 0,
                                        child: Container(
                                          width:
                                              MediaQuery.of(context).size.width,
                                          color: Colors.black.withOpacity(.5),
                                          child: Row(
                                            children: [
                                              Padding(
                                                padding:
                                                    const EdgeInsets.all(8.0),
                                                child: Text(movie.title,
                                                    style: const TextStyle(
                                                        fontSize: 12,
                                                        color: Colors.white)),
                                              ),
                                            ],
                                          ),
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                              );
                            },
                          )),
                    ),
                  ],
                ),
              ),
            ),
            FloatingSearchBar(
              hint: 'Search your favorite movie...',
              scrollPadding: const EdgeInsets.only(top: 16, bottom: 56),
              transitionDuration: const Duration(milliseconds: 500),
              transitionCurve: Curves.easeInOut,
              physics: const BouncingScrollPhysics(),
              axisAlignment:
                  MediaQuery.of(context).orientation == Orientation.portrait
                      ? 0.0
                      : -1.0,
              openAxisAlignment: 0.0,
              width: MediaQuery.of(context).orientation == Orientation.portrait
                  ? 600
                  : 500,
              debounceDelay: const Duration(milliseconds: 300),
              onSubmitted: controller.search,
              transition: CircularFloatingSearchBarTransition(),
              actions: [
                FloatingSearchBarAction(
                    showIfOpened: false,
                    child: CircularButton(
                      icon: const Icon(Icons.search),
                      onPressed: () {},
                    )),
                FloatingSearchBarAction.searchToClear(showIfClosed: false),
              ],
              builder: (context, transition) {
                return Obx(
                  () => ClipRRect(
                    borderRadius: BorderRadius.circular(8),
                    child: Material(
                      color: Colors.white,
                      elevation: 4.0,
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        children: controller.searchList.map(
                          (movie) {
                            return GestureDetector(
                              onTap: () =>
                                  configModalBottomSheet(context, movie),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Padding(
                                    padding: const EdgeInsets.all(8.0),
                                    child: Container(
                                      width: 100 / 2,
                                      height: 150 / 2,
                                      child: Image.network(
                                        movie.getPosterPath,
                                        fit: BoxFit.cover,
                                      ),
                                    ),
                                  ),
                                  Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      SizedBox(
                                        width:
                                            MediaQuery.of(context).size.width /
                                                2,
                                        child: Padding(
                                          padding: const EdgeInsets.all(8.0),
                                          child: Text(
                                            movie.title,
                                            style: GoogleFonts.poppins(
                                                fontSize: 12,
                                                color: Colors.black,
                                                fontWeight: FontWeight.bold),
                                          ),
                                        ),
                                      ),
                                    ],
                                  )
                                ],
                              ),
                            );
                          },
                        ).toList(),
                      ),
                    ),
                  ),
                );
              },
            ),
          ],
        ),
      ),
    );
  }
}
