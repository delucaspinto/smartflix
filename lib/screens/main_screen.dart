import 'package:app_movies/controllers/main_controller.dart';
import 'package:app_movies/screens/home_screen.dart';
import 'package:app_movies/screens/profile_screen.dart';
import 'package:app_movies/screens/search_screen.dart';
import 'package:custom_navigation_bar/custom_navigation_bar.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class MainScreen extends GetWidget<MainController> {
  const MainScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    List<Widget> _screens = [
      const HomeScreen(),
      const SearchScreen(),
      const ProfileScreen(),
    ];
    return Scaffold(
        // extendBody for floating bar get better perfomance
        extendBody: true,
        backgroundColor: Colors.white,
        bottomNavigationBar: Obx(
          () => CustomNavigationBar(
            iconSize: 30.0,
            selectedColor: Colors.white,
            strokeColor: Colors.white,
            unSelectedColor: Colors.grey[600],
            backgroundColor: Colors.black,
            blurEffect: true,
            opacity: 0.8,
            items: [
              CustomNavigationBarItem(
                icon: const Icon(Icons.home),
              ),
              CustomNavigationBarItem(
                icon: const Icon(Icons.search),
              ),
              CustomNavigationBarItem(
                icon: const Icon(Icons.person),
              ),
            ],
            currentIndex: controller.index.value,
            onTap: (index) {
              controller.index.value = index;
            },
            isFloating: false,
          ),
        ),
        body: Obx(() => _screens[controller.index.value]));
  }
}
