import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';

const Color primaryColor = Color.fromRGBO(0, 214, 239, 1);
const Color secondColor = Color.fromRGBO(0, 220, 170, 1);

Map<int, Color> color = {50: const Color.fromRGBO(0, 214, 239, 1)};

MaterialColor colorCustom = MaterialColor(50, color);
