import 'dart:developer';

import 'package:url_launcher/url_launcher.dart';

void launchURL(url) async {
  if (!await launch(url)) throw 'Could not launch $url';
  try {
    await launch(url);
  } catch (e) {
    log("$e");
  }
}

extension StringCasingExtension on String {
  String toCapitalized() =>
      length > 0 ? '${this[0].toUpperCase()}${substring(1).toLowerCase()}' : '';
  String toTitleCase() => replaceAll(RegExp(' +'), ' ')
      .split(' ')
      .map((str) => str.toCapitalized())
      .join(' ');
}
